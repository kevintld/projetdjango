from django.http.response import HttpResponseRedirect
from django.shortcuts import redirect, render
from Jeux_Video.models import Jeux_Video
from Media.models import Media
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout
from django.contrib import messages

def login(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(request, username=username, password=password)

        if user is not None:
            auth_login(request, user)
            messages.success(request, 'Vous êtes désormais connectés en tant que {0}.'.format(username))
        else:
            messages.error(request, 'Identifiants incorrects.')

    return HttpResponseRedirect(request.POST.get('next', '/'))


def logout(request):
    auth_logout(request)
    messages.info(request, 'Vous avez été déconnecté.')
    return HttpResponseRedirect(request.GET.get('next', '/'))


def accueil(request):
    films_populaires = Media.objects.filter(media_type = "movie").order_by("-note")[:6]
    series_populaires = Media.objects.filter(media_type = "tv").order_by("-note")[:6]
    jeux_populaires = Jeux_Video.objects.all().order_by("-note")[:6]
    
    return render(
        request,
        template_name='accueil.html',
        context={"films_populaires":films_populaires,"series_populaires":series_populaires, "jeux_populaires" : jeux_populaires}
    )

