from django.db import models
from django.core.paginator import Paginator

def getPagination(request, liste_objects, number_items):
    pagination = Paginator(liste_objects, number_items)
    num_page = request.GET.get('page')
    return pagination.get_page(num_page)
