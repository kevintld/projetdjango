from django.urls import path
from . import views

urlpatterns=[
    path('',views.liste,name='personne_liste'),
    path('details/<int:id>',views.details,name='personne_details'),
    path('creation',views.creationPersonne,name='personne_creation'),
    # path('/ajout/',views.ajout,name='personne_ajout'),
]