from django.contrib import admin
from Personne.models import Personne

class PersonneAdmin (admin. ModelAdmin ):
    list_display =('image','nom_prenom','biographie','apparitions_connues','genre','date_naissance','lieu_naissance')

admin.site.register(Personne , PersonneAdmin )
