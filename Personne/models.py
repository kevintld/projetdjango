from django.db import models
import requests
from bs4 import BeautifulSoup



class Personne(models.Model):
    id = models.IntegerField(primary_key=True)
    image = models.CharField(max_length=254,null=True)
    nom_prenom = models.CharField(max_length=254,null=True)
    biographie = models.TextField(null=True)
    apparitions_connues = models.IntegerField(null=True)
    genre = models.CharField(max_length=128,null=True)
    date_naissance = models.DateField(null=True)
    lieu_naissance = models.CharField(max_length=254,null=True)

'''
Récupère et insère une personne en base de donnée à partir d'une url donnée
'''
def scrapAndInsertPerson(person_url):
    base_url = "https://www.themoviedb.org"
    headers_compatibility = {"User-Agent" : "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36"}
    html = requests.get(person_url, headers=headers_compatibility)
    soup = BeautifulSoup(html.text, "lxml")

    id = getID(person_url, base_url)
    try:
        personne = Personne.objects.get(id=id)
    except:
        nom_prenom = ""
        essaie = 0

        while len(nom_prenom) < 2 and essaie <2 :
            nom_prenom = getNomPrenom(soup,person_url)
            personne = Personne(
                id=id,
                image=getImage(base_url,soup),
                nom_prenom=nom_prenom,
                biographie=getBiographie(soup),
                apparitions_connues=getInformationAside(soup, "Apparitions connues"),
                genre=getInformationAside(soup, "Genre"),
                date_naissance=getDateNaissance(soup),
                lieu_naissance=getInformationAside(soup, "Lieu de naissance"),
            )
        if len(nom_prenom)>2 :
            personne.save()

    return personne.pk

'''
Récupère l'ID de la personne'
'''
def getID(url, base_url):
    person_url = url[len(base_url):]
    person_url = person_url[len("/person/"):person_url.index("-")]
    return person_url

'''
Récupère le nom et le prénom de l'acteur
'''
def getNomPrenom(soup,person_url):
    nom = soup.select_one("section > div.title > h2.title > a")
    if nom :
        return nom.text.rstrip().lstrip()
    #return person_url[person_url.index("-")+1: -len('?language=fr')]
    return ""

'''
Récupère la photo de l'acteur
'''
def getImage(base_url, soup):
    image =  soup.select_one("div.image_content > img")
    if image :
        return base_url + image.get("data-src")
    return None

'''
Récupère la biographie de l'acteur
'''
def getBiographie(soup):
    biographie =soup.select_one("div.text.initial > p")
    if biographie :
        return soup.select_one("div.text.initial > p").text.rstrip().lstrip()
    return None


'''
Récupère une information du aside :
Apparitions connues
Genre
Date de naissance
Lieu de naissance
'''
def getInformationAside(soup, information):
    aside = soup.select("section.facts.left_column p strong bdi")
    for element in aside:
        if element.parent.text.rstrip().lstrip() == information:
            return element.parent.next_sibling.text.rstrip().lstrip()

'''
Renvoie la date de naissance correctement textualisé
'''
def getDateNaissance(soup):
    date_naissance = getInformationAside(soup, "Date de naissance")
    return date_naissance[:10] if date_naissance and date_naissance != '-' else None

class ApercuPersonne :
    def __init__(self,id,linkImage,saved,nom) :
        self.id =id 
        self.linkImage =linkImage 
        self.saved = saved
        self.nom =nom 
    def get_nom(self) :
        return self.nom

def recherche(recherche) :

    listPersonne = list()

    headers_compatibility = {"User-Agent" : "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36"}
    UNI = requests.get(" https://www.themoviedb.org/search/person?language=fr&query="+str(recherche), headers=headers_compatibility)
    soup = BeautifulSoup (UNI.text , "lxml")

    personnes = soup.select("div.item.profile")

    for personne in personnes :
        details = personne.find("p", { "class" : "name" }).find("a")
        nom = details.text
        id = details["href"][len("/person/"):]
        id = id.replace('?language=fr','')

       
        img = personne.find("div", { "class" : "image_content" }).find("img")
        if img :
            img = "https://www.themoviedb.org"+img["src"].replace("w90_and_h90_face","w300_and_h450_bestv2")
        else :
            img = ""

        try:
            media = Personne.objects.get(id=id)
            saved = True
        except :
            saved = False

        listPersonne.append(ApercuPersonne(id,img,saved,nom))
    return listPersonne