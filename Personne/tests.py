from django.test import TestCase
from selenium import webdriver
import time
from Personne.models import Personne


class PersonneTest(TestCase):

    def test_Personne(self):

        """
        ======================== AJOUT ========================
        """
        browser = webdriver.Chrome()
        browser.get('http://localhost:8000/personnes/')
        time.sleep(1)

        log = browser.find_element_by_css_selector('#navbar > div > button')
        username = browser.find_element_by_css_selector("#username")
        pwd = browser.find_element_by_css_selector("#password")

        log.click()
        time.sleep(1)

        username.send_keys("root")
        time.sleep(1)

        pwd.send_keys("toor")
        time.sleep(1)
    
        browser.find_element_by_css_selector("#connexionModal button[form='form-connexion']").click()
        time.sleep(1)

        browser.find_element_by_css_selector("#filtresHeaders + a").click()
        time.sleep(1)

        browser.find_element_by_css_selector("div.container > form > div > div:first-child label").click()
        time.sleep(1)

        personne = browser.find_element_by_css_selector("div.container > form > div > div:first-child h5.card-title").text

        browser.find_element_by_css_selector("div.container > form button[type=submit]").click()
        time.sleep(1)
        
        titre = browser.find_element_by_css_selector("div.container > div.row > div h2").text

        self.assertEqual(titre,personne)

        """
        ======================== EDITION ========================
        """
        time.sleep(1)
        browser.find_element_by_css_selector("div.container > div.row > div h2 > button:first-of-type").click()
        time.sleep(2)

        browser.find_element_by_css_selector("#id_nom_prenom").clear()
        time.sleep(1)
        browser.find_element_by_css_selector("#id_nom_prenom").send_keys("Un nouveau nom")
        time.sleep(1)

        browser.find_element_by_css_selector("#editPersonneModal button[type=submit]").click()
        time.sleep(1)

        new_titre = browser.find_element_by_css_selector("div.container > div.row > div h2").text
        
        self.assertEqual("Un nouveau nom",new_titre)
        
        """
        ======================== SUPPRESSION ========================
        """

        time.sleep(1)
        browser.find_element_by_css_selector("div.container > div.row > div h2 > button:last-of-type").click()
        time.sleep(1)

        browser.find_element_by_css_selector("#removePersonneModal button[type=submit]").click()
        time.sleep(1)

        self.assertEqual(
            len(Personne.objects.filter(nom_prenom=new_titre)),
            0
        )

        browser.quit()
