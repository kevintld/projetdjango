# Generated by Django 3.2.8 on 2021-10-14 22:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Personne', '0005_alter_personne_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='personne',
            name='apparitions_connues',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='personne',
            name='genre',
            field=models.CharField(max_length=128, null=True),
        ),
        migrations.AlterField(
            model_name='personne',
            name='lieu_naissance',
            field=models.CharField(max_length=254, null=True),
        ),
        migrations.AlterField(
            model_name='personne',
            name='nom_prenom',
            field=models.CharField(max_length=254, null=True),
        ),
    ]
