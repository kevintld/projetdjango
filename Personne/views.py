from django.forms.widgets import SelectDateWidget
from django.http.response import HttpResponseRedirect
from django.shortcuts import redirect, render
from django.contrib import messages
from django.forms.models import ModelForm
from Personne.models import Personne,recherche,ApercuPersonne,scrapAndInsertPerson
from Media.models import Media
from Mediatheque.utils import getPagination
from django.urls import reverse
from django.contrib.admin.views.decorators import staff_member_required
import datetime

class PersonneForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PersonneForm , self).__init__(*args, **kwargs)
        self.fields["nom_prenom"].label = "Nom et Prénom"
        self.fields["biographie"].label = "Biographie"
        self.fields["biographie"].required = False
        self.fields["apparitions_connues"].label = "Nombre d'apparitions connues"
        self.fields["apparitions_connues"].required = False
        self.fields["genre"].label = "Genre"
        self.fields["genre"].required = False
        self.fields["date_naissance"].label = "Date de naissance"
        self.fields["date_naissance"].required = False
        self.fields["date_naissance"].widget = SelectDateWidget(years=[i for i in reversed(range(1800,datetime.datetime.now().year))])
        self.fields["lieu_naissance"].label = "Lieu de naissance"
        self.fields["lieu_naissance"].required = False

    class Meta:
        model = Personne
        fields = ('nom_prenom','genre','biographie','date_naissance', 'lieu_naissance','apparitions_connues')


def liste(request):
    recherche = request.GET.get("recherche")
    if recherche:
        personnes = Personne.objects.filter(nom_prenom__icontains=recherche)
    else:
        personnes = Personne.objects.all()

        filtre = request.GET.get("critere")
        if filtre:
            personnes = personnes.order_by(filtre)

    return render(
        request,
        template_name='Personne/liste.html',
        context={'page': getPagination(request, personnes, 6*8)} 
    )


def details(request, id):
    participations = list()
    participations.extend(Media.objects.filter(keys_travailleurs__contains="/{0}/".format(id)))
    participations.extend(Media.objects.filter(keys_interpretation__contains="/{0}/".format(id)))

    personne = Personne.objects.get(pk=id)

    form = PersonneForm(instance=personne)
    
    if request.method == "POST" :
        form = PersonneForm(request.POST, instance=personne)

        method = request.POST.get("_method").lower()

        if method == "put" and form.is_valid():
            form.save()
            messages.success(request, 'La personne ' + personne.nom_prenom + ' a bien été modifiée.' )
            return redirect(reverse('personne_details', args=[personne.pk]))

        elif method == "delete":
            personne.delete()
            messages.error(request, "La personne a été supprimée.")
            return redirect(reverse('personne_liste'))

    return render(
        request,
        template_name='Personne/detail.html',
        context = {'personne': personne, "participations":participations, "form":form} 
    )


@staff_member_required
def creationPersonne(request) :
    personnes = ""
    elem_recherche = request.GET.get("recherche")
    personnes = recherche(elem_recherche)

    filtre = request.GET.get("critere")
    personnes.sort(key=ApercuPersonne.get_nom, reverse=filtre=="True")

    if request.method == "POST":
        ajouter = request.POST.getlist("ajouter")

        for i in range(len(ajouter)):    
            linkAjout = "https://www.themoviedb.org/person/"+ ajouter[i] + "-h?language=fr"
            scrapAndInsertPerson(linkAjout)

            if len(ajouter) == 1:
                return redirect("personne_details", id=ajouter[i])
        
        return HttpResponseRedirect(request.POST.get("next", "/"))

    return render(
        request,
        template_name='Personne/creation.html',
        context={'personnes' : personnes} 

    )