# !/bin/bash

if [ $1 ]
then
    mysql -u talland -h servinfo-mariadb -p DBtalland --password=talland
    
    drop table Jeux_Video_jeux_video;
    drop table Media_media;
    drop table Personne_personne;
    drop table auth_group_permissions;
    drop table auth_user_groups;
    drop table auth_user_user_permissions;
    drop table django_admin_log;
    drop table django_content_type;
    drop table django_migrations;
    drop table django_session;

    drop table auth_group;
    drop table auth_permission;
    drop table auth_user;
    drop table django_content_type;
    drop table django_session;
else
    mysql -u talland -h servinfo-mariadb -p DBtalland --password=talland
fi