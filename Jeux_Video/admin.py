from django.contrib import admin
from Jeux_Video.models import Jeux_Video

class Jeux_VideoAdmin( admin.ModelAdmin ):
    list_display =('titre', 'genres', 'date_de_sortie','producteur','link_img_sm')

admin.site.register(Jeux_Video , Jeux_VideoAdmin )