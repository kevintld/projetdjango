from django.core.management.base import BaseCommand, CommandError
from Jeux_Video.models import *

class Command(BaseCommand):
    help = 'Adds a user to django'

    def add_arguments(self, parser):
        parser.add_argument('lien')

    def handle(self, *args, **options):
        scrapJV(options['lien'])
        # getInfoJV(options['lien'])