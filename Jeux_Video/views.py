from django.forms.models import ModelForm
from django.http.response import HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render
from Jeux_Video.models import Jeux_Video,recherche,JvApercu
from Mediatheque.utils import getPagination
from Jeux_Video.models import getInfoJV
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib import messages
from django.urls import reverse

class JVForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(JVForm , self).__init__(*args, **kwargs)
        self.fields['titre'].label = "Titre du jeu"
        self.fields['producteur'].label = "Producteur/Dev du jeu"
        self.fields['producteur'].required = False
        self.fields['distributeur'].label = "Distributeur du Jeu"
        self.fields['distributeur'].required = False
        self.fields['plateformes'].label = "Plateformes disponibles"
        self.fields['plateformes'].required = False
        self.fields['genres'].label = "Genres"
        self.fields['genres'].required = False
        self.fields['synopsis'].label = "Résumé du jeu"
        self.fields['synopsis'].required = False
        self.fields['date_de_sortie'].label = "Date de sortie"
        self.fields['date_de_sortie'].required = False

    class Meta:
        model = Jeux_Video
        fields = ('titre','producteur','distributeur','plateformes', 'genres','synopsis','date_de_sortie')

    
def liste(request):
    genres = ["Stratégie temps réel","Action","Aventure","RPG","Stratégie tour par tour","Action-Aventure","FPS","Visual Novel","Roguelike","plateforme","infiltration"]

    jeux = Jeux_Video.objects.all()
    filtre = request.GET.get("critere")
    genre_choisie = request.GET.get("genres")
    type = request.GET.get("type")
    recherche = request.GET.get("recherche")

    if genre_choisie:
        jeux = jeux.filter(genres__icontains=genre_choisie)

    if type:
        jeux = jeux.filter(jeu_type=type)

    if recherche:
        jeux = jeux.filter(titre__icontains=recherche)

    if filtre:
        jeux = jeux.order_by(filtre)
        
    return render(
        request,
        template_name='JeuxVideo/liste.html',
        context={
            'page': getPagination(request, jeux, 3*5),
            'genres':genres
        } 
    )


def details(request, id):
    jeu = get_object_or_404(Jeux_Video, pk=id)

    form = JVForm(instance=jeu)

    if request.method == "POST" :
        form = JVForm(request.POST, instance=jeu)

        method = request.POST.get("_method").lower()

        if method == "put" and form.is_valid():
            form.save()
            messages.success(request, 'Le jeu ' + jeu.titre + ' a bien été modifié.' )
            return redirect(reverse('jv_details', args=[jeu.pk]))

        elif method == "delete":
            jeu.delete()
            messages.error(request, "Le jeu a été supprimé.")
            return redirect(reverse('jv_liste'))

    return render(
        request,
        template_name='JeuxVideo/detail.html',
        context={'jeu': jeu, "form":form} 
    )


@staff_member_required
def creationJeu(request) :
    jeux = ""
    elem_recherche = request.GET.get("recherche")
    jeux = recherche(elem_recherche)

    filtre = request.GET.get("critere")
    jeux.sort(key=JvApercu.get_titre, reverse=filtre=="True")
    
    if(request.method == "POST") :
        ajouter = request.POST.getlist("ajouter")
        
        for i in range(len(ajouter)):
            jeu_lien = request.POST.get("jeu_lien_{0}".format(ajouter[i]))
            jeu_lien = jeu_lien.replace("https://www.senscritique.com","")
            getInfoJV(jeu_lien)

            if len(ajouter) == 1:
                return redirect("jv_details", id=ajouter[i])
        
        return HttpResponseRedirect(request.POST.get("next", "/"))

    return render(
        request,
        template_name='JeuxVideo/creation.html',
        context={'jeux' : jeux} 
    )