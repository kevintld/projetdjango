from django.apps import AppConfig


class JeuxVideoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Jeux_Video'
