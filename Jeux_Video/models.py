from django.db import models
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
# from webdriver_manager.chrome import ChromeDriverManager
# from webdriver_manager.utils import ChromeType
import requests
import time
import json

class Jeux_Video(models.Model):
    id=models.IntegerField(primary_key=True)
    note = models.IntegerField()
    titre  = models.CharField(max_length=128)
    producteur = models.CharField(max_length=128) #many to one
    distributeur = models.CharField(max_length=128) #many to one
    plateformes=models.CharField(max_length=128) #many to many
    genres=models.CharField(max_length=128) #many to many
    synopsis = models.TextField()
    date_de_sortie=models.CharField(max_length=32)
    link_img_sm=models.TextField(default="")
    link_img_lg=models.TextField(default="")
    link_vid=models.TextField(default="",null=True)

    

#retourne une liste de lien en fonction d'une url, 
def ListeLien(urlListe):
    ListeJV = requests.get(urlListe)
    soup = BeautifulSoup(ListeJV.text, "lxml")
    list_lien = []
    for lien in soup.find_all("a", class_="elco-anchor"):
        if lien.has_attr('href'):
            list_lien.append(lien.attrs["href"])
    return list_lien

#a partir d'un lien unique enregistre dans la BD 
# les informations suivant le modèle JV
def getInfoJV(urlJV):
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    browser = webdriver.Chrome(chrome_options=chrome_options)
    browser.get("https://www.senscritique.com"+urlJV)
    browser.execute_script('if (document.querySelector("button[data-rel=sc-btn-more-storyline]")) document.querySelector("button[data-rel=sc-btn-more-storyline]").click()')
    time.sleep(1)

    soup = BeautifulSoup(browser.page_source, "lxml")
    browser.quit()

    titre = producteur = distributeur = plateformes = genres = synopsis = date_de_sortie = link_img_sm = link_img_lg = link_vid =  ""
    note = 0

    titre = soup.find("h1", class_="pvi-product-title").text
    titre=titre.rstrip().lstrip()

    créateurs=soup.find_all("span",itemprop="creator")
    if (len(créateurs)>=1):
        producteur=créateurs[0].text.rstrip().lstrip()
    if (len(créateurs)>=2):
        distributeur=créateurs[1].text.rstrip().lstrip()

    #récupération des plateformes
    temp=soup.select("li.pvi-productDetails-item:nth-child(2)")
    if(temp):
        for plateforme in temp:
            plateformes+=(plateforme.text.rstrip().lstrip())
    #genre
    temp=soup.find_all("span",itemprop="genre")
    if(temp):
        for genre in temp:
            genres+=(genre.text.rstrip().lstrip())+" "
    #synopsis
    temp = soup.select_one('p.pvi-productDetails-resume[data-rel="full-resume"]') if soup.select_one('p.pvi-productDetails-resume[data-rel="full-resume"]') else soup.select_one('p.pvi-productDetails-resume[data-rel="small-resume"]') 
    if (temp):
        synopsis = temp.text.rstrip().lstrip()
        if synopsis[-6:] == "Fermer":
            synopsis = synopsis[:-6]

    #date de sortie
    temp=soup.select_one("li.pvi-productDetails-item:nth-child(4) > time:nth-child(1)")
    if (temp):
        date_de_sortie=temp.text
    else:
        temp=soup.select_one("li.pvi-productDetails-item:nth-child(4)")
        date_de_sortie=temp.text

    #scrap petite img 
    temp= soup.find("img", class_="pvi-hero-poster") 
    if (temp):
        link_img_sm =temp['src']

    #scrap grande img 
    temp= soup.find("a", class_="lightview")
    if (temp):
        link_img_lg =temp['href']
     
    #scrap video
    pageVideo=requests.get("https://www.senscritique.com"+urlJV+"/videos")
    soupVid = BeautifulSoup(pageVideo.text, "lxml")

    temp_id=soupVid.select_one("div.pvid-focus")
    if (temp_id):
        idytb=temp_id.get("data-sc-video-id")
        link_vid="https://www.youtube.com/embed/"+idytb

    
    temp = soup.select_one("div.pvi-product-scrating > span")
    if temp :
        note = int(temp.text.replace(".",""))

    #scrap id
    tabtemp= urlJV.split("/")
    id=tabtemp[-1]



    #enregistrement
    try: 
        monJV = Jeux_Video.objects.get(id=id)
    except:
        monJV = Jeux_Video(
            id=id,
            titre = titre,
            producteur=producteur,
            distributeur=distributeur,
            plateformes=plateformes,
            genres=genres,
            synopsis=synopsis,
            date_de_sortie=date_de_sortie,
            link_img_sm=link_img_sm,
            link_img_lg=link_img_lg,
            link_vid=link_vid,
            note = note
        )
        monJV.save()



#fonction Général de scrapping
def scrapJV(link):
    listeLink=ListeLien(link)
    for lien in listeLink:     
         getInfoJV(lien)

#remplissage de la bd avec le top 100 des top 10
def remplissage():
    scrapJV("https://www.senscritique.com/jeuxvideo/tops/top100-des-top10")

class JvApercu :
    def __init__(self,id,img,titre,lien,date,saved) :
        self.id = id
        self.img =img
        self.titre =titre
        self.lien =lien
        self.date =date
        self.saved =saved
        
    def get_titre(self) :
        return self.titre

def recherche(recherche) :

    listJeu = list()

    headers_compatibility = {"User-Agent" : "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36"}
    UNI = requests.get("https://www.senscritique.com/search?q="+str(recherche)+"&categories[0][0]=Jeux", headers=headers_compatibility)
    soup = BeautifulSoup (UNI.text , "lxml")

    jeux = json.loads(soup.find('script', type='application/json').text)   

    for jeu in jeux["props"]["pageProps"]["results"]["hits"]["hits"] :
        img = jeu.get('_source').get('cover').replace("95x130","source_big")
        titre =jeu.get('_source').get('title')
        lien = jeu.get('_source').get('url')
        date = jeu.get('_source').get('year')
        id =jeu.get('_source').get('id')
       

        monJV = Jeux_Video.objects.filter(
            titre=titre,
        )
        saved = False
        if monJV :
            saved = True
        listJeu.append(JvApercu(id,img,titre,lien,date,saved))


    return listJeu