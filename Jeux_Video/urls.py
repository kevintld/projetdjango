from django.urls import path
from . import views

urlpatterns=[
    path('',views.liste,name='jv_liste'),
    path('details/<int:id>',views.details,name='jv_details'),
    path('creation',views.creationJeu,name='jv_creation'),
]