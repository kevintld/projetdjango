from django.test import TestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from Jeux_Video.models import Jeux_Video
import time
import re

class NewJeuxVideoTest(TestCase):

    '''Test d'ajout,modification et supression d'un JV '''
    def test_add_JeuVideo(self):

        #ajout d'un utilisateur pour creation de tache
        browser = webdriver.Chrome()
        browser.get('http://localhost:8000/jeuxvideo/')
        time.sleep(1)

        log = browser.find_element_by_css_selector('#navbar > div > button')
        username = browser.find_element_by_css_selector("#username")
        pwd = browser.find_element_by_css_selector("#password")

        log.click()
        time.sleep(1)

        username.send_keys("root")
        time.sleep(1)

        pwd.send_keys("toor")
        time.sleep(1)
    
        browser.find_element_by_css_selector("#connexionModal > div > div > div.modal-footer > button").click()
        time.sleep(1)

        #creation d'un JV
        browser.find_element_by_css_selector("#filtres > div.d-flex.flex-row.flex-wrap.justify-content-between.align-items-center > div > a").click()

        time.sleep(1)

        recherche= browser.find_element_by_css_selector("input.form-control:nth-child(2)")
        recherche.send_keys("Assassin creed")
        time.sleep(1)

        #Recherche
        browser.find_element_by_css_selector(".btn-outline-success").click()
        #clique ajout
        titre=browser.find_element_by_css_selector("div.col-12:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > h5:nth-child(1)").text 
        browser.find_element_by_css_selector("div.col-12:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > label:nth-child(3)").click()
        browser.find_element_by_css_selector(".btn-success").click()
        time.sleep(8)

        #verif ajout
        re.match(titre,browser.find_element_by_css_selector(".mb-5").text )
        
        #modification
          
        browser.find_element_by_css_selector("button.m-2:nth-child(1)").click()
        time.sleep(1)
        titremodal=browser.find_element_by_css_selector("#id_titre")
        titremodal.send_keys("2")
        titre=browser.find_element_by_css_selector("#id_titre").text
        time.sleep(1)
        browser.find_element_by_css_selector("#editJVModal > div:nth-child(1) > div:nth-child(1) > div:nth-child(3) > button:nth-child(1)").click()
        time.sleep(1)

        #verif modification
        re.match(titre,browser.find_element_by_css_selector(".mb-5").text )

        #suppresion
        browser.find_element_by_css_selector("button.m-2:nth-child(2)").click()
        time.sleep(1)
        browser.find_element_by_css_selector(".btn-danger").click()
        time.sleep(1)
        #verif suppresion
        self.assertEquals(len(Jeux_Video.objects.filter(titre=titre)),0)
        
        browser.quit()

