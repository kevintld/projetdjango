from  django.urls import resolve
from django.test import TestCase

class HomePageTestContent(TestCase):
    
    routes = {
    "/" :  "Mediatheque.views.accueil",
    "/medias/" :  "Media.views.liste",
    "/medias/details/11" :  "Media.views.details",
    "/medias/creation" :   "Media.views.creationMedia",

    "/personnes/" :  "Personne.views.liste",
    "/personnes/details/11" :  "Personne.views.details",
    "/personnes/creation" :   "Personne.views.creationPersonne",
    
    "/jeuxvideo/" :  "Jeux_Video.views.liste",
    "/jeuxvideo/details/11" :  "Jeux_Video.views.details",
    "/jeuxvideo/creation" :   "Jeux_Video.views.creationJeu",

    }


    '''Test unitaire de la page accueil sur la racine du projet'''
    def test_root_url_resolves_to_home_view(self):
        for route,function in self.routes.items():
            found = resolve(route)
            self.assertEqual(found._func_path,function)
