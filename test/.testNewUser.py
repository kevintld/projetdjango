from django.test import TestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

import time
import re

class NewUserTest(TestCase):

    '''Test d'ajout et supression d'une personne '''
    def test_add_user(self):

        browser = webdriver.Chrome()
        browser.get('http://localhost:8000/user/create')
        time.sleep(1)

        name = browser.find_element_by_id("id_name")
        firstname = browser.find_element_by_id("id_firstname")
        email = browser.find_element_by_id("id_email")

        name.send_keys("Pas")
        time.sleep(1)

        firstname.send_keys("Pascal")
        time.sleep(1)

        email.send_keys("Pas.Pascal@papascal.com")
        time.sleep(1)

        browser.find_element_by_id("submit").click()
        time.sleep(1)

        #Là la personne doit etre crée
        noms = browser.find_elements_by_css_selector("#nom")
        time.sleep(1)
        userFound = False
        elem = None
        for nom in noms:
            if( re.match("Pas Pascal", nom.text) ):
                userFound = True
                elem = nom

        #la personne est crée
        self.assertEqual(True,userFound)

        #mnt on va la supprimer
        browser.find_element_by_css_selector("button[name=\"effacer\"]").click()
        time.sleep(1)

        #on verrifie qu'elle est supprimée
        noms = browser.find_elements_by_css_selector("h5.nom")
        userFound = False
        for nom in noms:
            if( re.match("Pas Pascal", nom.text) ):
                userFound = True

        #la personne est crée
        self.assertEqual(False,userFound)
        time.sleep(1)
        

        browser.quit()
