from django.test import TestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

import time
import re

class NewTaskTest(TestCase):

    '''Test d'ajout et supression d'une tache '''
    def test_add_task(self):

        #ajout d'un utilisateur pour creation de tache
        browser = webdriver.Chrome()
        browser.get('http://localhost:8000/user/create')
        time.sleep(1)

        name = browser.find_element_by_id("id_name")
        firstname = browser.find_element_by_id("id_firstname")
        email = browser.find_element_by_id("id_email")

        name.send_keys("Pah")
        time.sleep(1)

        firstname.send_keys("Patrick")
        time.sleep(1)

        email.send_keys("Pah.Patrick@papatrick.com")
        time.sleep(1)

        browser.find_element_by_id("submit").click()
        time.sleep(1)

        #creation d'une tache
        browser.find_element_by_css_selector("#crea-tache").click()

        time.sleep(1)

        name = browser.find_element_by_id("id_name")
        desc = browser.find_element_by_id("id_description")
        user = Select(browser.find_element_by_id("id_user"))

        name.send_keys("Ma super tache")
        time.sleep(1)

        desc.send_keys("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.")
        time.sleep(1)

        user.select_by_index(1)
        time.sleep(1)

        browser.find_element_by_id("submit").click()
        time.sleep(1)

        #Là la tache doit etre crée
        noms = browser.find_elements_by_css_selector("#nom")
        time.sleep(1)
        taskFound = False
        elem = None
        for nom in noms:
            if( re.match("Ma super tache", nom.text) ):
                taskFound = True
                elem = nom

        #la tache est crée
        self.assertEqual(True,taskFound)

        #mnt on va la supprimer
        browser.find_element_by_css_selector("button[name=\"effacer\"]").click()
        time.sleep(1)

        #on verrifie qu'elle est supprimée
        noms = browser.find_elements_by_css_selector("h5.nom")
        taskFound = False
        for nom in noms:
            if( re.match("Ma super tache", nom.text) ):
                taskFound = True

        #la tache est crée
        self.assertEqual(False,taskFound)
        time.sleep(1)
        

        browser.quit()
