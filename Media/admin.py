from django.contrib import admin
from Media.models import Media

class MediaAdmin( admin.ModelAdmin ):
    list_display =('titre', 'genre', 'date_sortie', 'duree', 'note')

admin.site.register(Media , MediaAdmin )
