from django.core.management.base import BaseCommand, CommandError
from Media.models import *

class Command(BaseCommand):
    help = 'Adds a user to django'

    def add_arguments(self, parser):
        parser.add_argument('type')
        parser.add_argument('page')

    def handle(self, *args, **options):
        scrapMediaLink(options['type'],options['page'])