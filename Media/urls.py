from django.urls import path
from . import views

urlpatterns=[
    path('',views.liste,name='media_liste'),
    path('details/<int:id>',views.details,name='media_details'),
    path('creation',views.creationMedia,name='media_creation'),
    # path('/details/<int:pk>',views.details,name='personne_details'),
    # path('/ajout/',views.ajout,name='personne_ajout'),
]