# Generated by Django 3.2.8 on 2021-10-14 22:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Media', '0003_auto_20211014_1503'),
    ]

    operations = [
        migrations.AlterField(
            model_name='media',
            name='budget',
            field=models.CharField(max_length=128, null=True),
        ),
        migrations.AlterField(
            model_name='media',
            name='id',
            field=models.IntegerField(primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='media',
            name='recette',
            field=models.CharField(max_length=128, null=True),
        ),
    ]
