import json
from django.http.response import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.contrib import messages
from django.forms.models import ModelForm
from Media.models import Media, recherche,affiche,scrapMediaDetail
from Mediatheque.utils import getPagination
from django.shortcuts import get_object_or_404
from Personne.models import Personne
from Media.models import scrapMediaDetail
from django.contrib.admin.views.decorators import staff_member_required
from django.urls import reverse

class MediaForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(MediaForm , self).__init__(*args, **kwargs)
        self.fields["titre"].label = "Titre"
        self.fields["date_sortie"].label = "Date de sortie"
        self.fields["date_sortie"].required = False
        self.fields["age_reco"].label = "Âge recommandé"
        self.fields["age_reco"].required = False
        self.fields["genre"].label = "Genre"
        self.fields["genre"].required = False
        self.fields["duree"].label = "Durée"
        self.fields["duree"].required = False
        self.fields["note"].label = "Note"
        self.fields["note"].required = False
        self.fields["synopsis"].label = "Synopsis"
        self.fields["synopsis"].required = False
        self.fields["budget"].label = "Budget"
        self.fields["budget"].required = False
        self.fields["recette"].label = "Recette"
        self.fields["recette"].required = True
        self.fields["media_type"].label = "Type de media"
        self.fields["media_type"].required = True
       
    class Meta:
        model = Media
        fields = ('titre','date_sortie','age_reco','genre','duree','note','synopsis','budget','recette','media_type')

def liste(request):
    genres = ["Action","Aventure","Comédie","Horreur","Fantastique","Science-Fiction","Documentaire"]

    medias = Media.objects.all()
    filtre = request.GET.get("critere")
    genre_choisie = request.GET.get("genre")
    type = request.GET.get("type")
    recherche = request.GET.get("recherche")

    if genre_choisie:
        medias = medias.filter(genre__icontains=genre_choisie)

    if type:
        medias = medias.filter(media_type=type)

    if recherche:
        medias = medias.filter(titre__icontains=recherche)

    if filtre:
        medias = medias.order_by(filtre)
        
    return render(
        request,
        template_name='Media/liste.html',
        context={
            'page': getPagination(request, medias, 6*5),
            'genres':genres
        } 
    )

def details(request, id):
    media = get_object_or_404(Media, pk=id)

    interpretation =  json.loads(media.interpretation)
    travaille =  json.loads(media.travailleurs)

    interpretes = dict()
    travailleurs = dict()

    for key,role in interpretation.items() :
        try:
            personne = Personne.objects.get(pk=key)
            interpretes[role] = personne
        except:
            pass
            

    for key,role in travaille.items() :
        try : 
            personne = Personne.objects.get(pk=key)
            travailleurs[role] = personne
        except : 
            pass

    form = MediaForm(instance=media)
    if request.method == "POST" :
        form = MediaForm(request.POST, instance=media)

        method = request.POST.get("_method").lower()

        if method == "put" and form.is_valid():
            form.save()
            messages.success(request, 'Le média ' + media.titre + ' a bien été modifié.' )
            return redirect(reverse('media_details', args=[media.pk]))

        elif method == "delete":
            media.delete()
            messages.error(request, "Le média a été supprimé.")
            return redirect(reverse('media_liste'))
            
    return render(
        request,
        template_name='Media/detail.html',
        context={'media': media, 'interpretation' : interpretes, 'travailleurs' : travailleurs, "form":form} 
    )


@staff_member_required
def creationMedia(request) :
    medias = ""
    tv = True
    movie = True
    type = request.GET.get("type")

    if type == "tv" :
        tv = True
        movie = False
    elif type == "movie" :
        tv = False
        movie = True

    elem_recherche = request.GET.get("recherche")
    medias = recherche(elem_recherche,movie,tv)
    
    filtre = request.GET.get("critere")
    
    medias.sort(key=affiche.get_titre, reverse=filtre=="True")

    if request.method == "POST":
        ajouter = request.POST.getlist("ajouter")

        for i in range(len(ajouter)):    
            type = request.POST.get("type_ajout_{0}".format(ajouter[i]))
            linkAjout = "https://www.themoviedb.org/"+ type +"/" + ajouter[i] + "?language=fr"
            scrapMediaDetail(linkAjout,type)

            if len(ajouter) == 1:
                return redirect("media_details", id=ajouter[i])
        
        return HttpResponseRedirect(request.POST.get("next", "/"))

    return render(
        request,
        template_name='Media/creation.html',
        context={'medias' : medias} 

    )
