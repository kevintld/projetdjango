from django.test import TestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from Media.models import Media

import time
import re

class NewMediaTest(TestCase):

    '''Test d'ajout modification et supression d'un Media '''
    def test_add_Media(self):

        #ajout d'un utilisateur pour creation de tache
        browser = webdriver.Chrome()
        browser.get('http://localhost:8000/medias/')
        time.sleep(1)

        log = browser.find_element_by_css_selector('#navbar > div > button')
        username = browser.find_element_by_css_selector("#username")
        pwd = browser.find_element_by_css_selector("#password")

        log.click()
        time.sleep(1)

        username.send_keys("root")
        time.sleep(1)

        pwd.send_keys("toor")
        time.sleep(1)

        browser.find_element_by_css_selector("#connexionModal > div > div > div.modal-footer > button").click()
        time.sleep(1)

        #creation d'une tache
        browser.find_element_by_css_selector("#filtres > div.d-flex.flex-row.flex-wrap.justify-content-between.align-items-center > div > a").click()

        time.sleep(1)
        cardTitle = browser.find_element_by_css_selector("body > div.container.mt-3 > form > div > div:nth-child(1) > div > div.card-footer.d-flex.flex-column.align-items-center.h-100.text-center > h5").text

        browser.find_element_by_css_selector("body > div.container.mt-3 > form > div > div:nth-child(1) > div > div.card-footer.d-flex.flex-column.align-items-center.h-100.text-center > label").click()
        
        browser.find_element_by_css_selector("body > div.container.mt-3 > form > button").click()

        time.sleep(3)

        titre = browser.find_element_by_css_selector("body > div.container.mt-3 > div:nth-child(1) > div > div > h2")
        
        re.match(cardTitle, titre.text)

        browser.find_element_by_css_selector("body > div.container.mt-3 > div:nth-child(1) > div > div > h2 > button.m-2.ms-4.btn.btn-outline-success").click()
        time.sleep(1)
        fieldTitre = browser.find_element_by_css_selector("#id_titre")
        fieldTitre.send_keys("test")


        browser.find_element_by_css_selector("#editMediaModal > div > div > div.modal-footer > button").click()
        time.sleep(1)

        titre = browser.find_element_by_css_selector("body > div.container.mt-3 > div:nth-child(1) > div > div > h2")
        re.match(cardTitle+"test", titre.text)

        time.sleep(1)
        browser.find_element_by_css_selector("body > div.container.mt-3 > div:nth-child(1) > div > div > h2 > button.m-2.btn.btn-outline-danger").click()
        time.sleep(1)
        browser.find_element_by_css_selector("#removeMediaModal > div > div > div.modal-footer > button").click()

        time.sleep(5)
        self.assertEqual(len(Media.objects.filter(titre=cardTitle+"test")),0)
        
        browser.quit()
