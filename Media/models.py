from django.db import models
import requests
from bs4 import BeautifulSoup
import json
from Personne.models import getInformationAside,scrapAndInsertPerson
import threading


class Media(models.Model):
    id = models.IntegerField(primary_key=True)
    linkBandeAnnonce = models.CharField(max_length=254)
    linkImage = models.CharField(max_length=254)
    date_sortie = models.IntegerField()
    age_reco  = models.CharField(max_length=10)
    genre = models.CharField(max_length=128)
    duree = models.CharField(max_length=50)
    note = models.IntegerField()
    synopsis = models.TextField()
    budget = models.CharField(max_length=128,null=True)
    recette = models.CharField(max_length=128,null=True)
    titre  = models.CharField(max_length=128)
    media_type = models.CharField(max_length=50)
    interpretation = models.TextField()
    keys_interpretation = models.TextField()
    travailleurs = models.TextField()
    keys_travailleurs = models.TextField()

def scrapMediaDetail (link,type) :

    headers_compatibility = {"User-Agent" : "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36"}
    UNI = requests.get(link, headers=headers_compatibility)
    soup = BeautifulSoup (UNI.text , "lxml")
    id = link[len('https://www.themoviedb.org/'+type+'/'):-len('?language=fr')]
    essaie = 0

    try:
        media = Media.objects.get(id=id)
    except:
        linkBandeAnnonce = linkImage = age_reco = genre = duree =  synopsis = budget = recette = titre = ""
        note = date_sortie = 0
        while len(titre) < 2 and essaie <2 :
            media_type = type
            interpretation = dict()
            travailleurs = dict()
            keys_travailleurs = keys_interpretation = '/'

            bandeAnnonce = soup.select_one("li.video.none > a")
            if(bandeAnnonce) :
                linkBandeAnnonce = "http://www.youtube.com/embed/" +bandeAnnonce.get('data-id')

            image = soup.select_one("div.image_content.backdrop > img")
            if(image) :
                linkImage = "https://www.themoviedb.org" +image.get('data-src')
            
            titre_tmp = soup.select_one("div.title > h2 > a")
            if titre_tmp :
                titre = titre_tmp.text

            date_sortie_tmp = soup.select_one('span.release_date')
            if date_sortie_tmp:
                date_sortie = date_sortie_tmp.text[1:-1]

            age_reco_tmp = soup.select_one("span.certification")
            if(age_reco_tmp) :
                age_reco = age_reco_tmp.text.rstrip().lstrip()

            genres = soup.select("span.genres")
            for g in genres :
                genre += g.text.rstrip().lstrip()

            
            runtime = soup.select_one("span.runtime")
            if(runtime) :
                duree = runtime.text.rstrip().lstrip()

            note_tmp = soup.select_one("div.user_score_chart")
            if(note_tmp) :
                note = note_tmp.get("data-percent")[:-2]

            synopsis_tmp = soup.select_one("div.overview > p ")
            if(synopsis_tmp) :
                synopsis = synopsis_tmp.text

            if(type == "movie") :
                budget = getInformationAside(soup,"Budget")
                recette = getInformationAside(soup,"Recette")

            

            acteurs = soup.select("ol.people >li.card > a")
            perso = soup.select("ol.people >li.card > p.character")
            for i in range(len(acteurs)) :
                linkPersonne = "https://www.themoviedb.org" + acteurs[i]["href"]
                key = scrapAndInsertPerson("https://www.themoviedb.org" + acteurs[i]["href"])
                nom_perso = perso[i].text.rstrip().lstrip()
                interpretation[key] = nom_perso
                keys_interpretation+=(str(key)+"/")

            interpretation = json.dumps(interpretation)    

            contributeurs = soup.select("ol.people >li.profile > p > a")
            role = soup.select("ol.people >li.profile > p.character")
            for i in range(len(contributeurs)) :
                linkPersonne = "https://www.themoviedb.org" + contributeurs[i]["href"]
                key = scrapAndInsertPerson(linkPersonne)
                tache =  role[i].text
                travailleurs[key] = tache
                keys_travailleurs+=(str(key)+"/")

            travailleurs = json.dumps(travailleurs)
            essaie += 1

                
        if(len(titre)>2) :
            media = Media(
                id = id,
                linkBandeAnnonce = linkBandeAnnonce,
                linkImage = linkImage,
                date_sortie = date_sortie,
                age_reco = age_reco,
                genre = genre,
                duree = duree,
                note = note,
                synopsis = synopsis,
                budget = budget,
                recette = recette,
                titre = titre,
                media_type = media_type,
                interpretation = interpretation,
                keys_interpretation = keys_interpretation,
                travailleurs = travailleurs,
                keys_travailleurs = keys_travailleurs
            )
            media.save()


'''
type : tv / movie
'''
def scrapMediaLink(type,page) :
    headers_compatibility = {"User-Agent" : "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36"}
    UNI = requests.get("https://www.themoviedb.org/"+type+"?language=fr&page="+str(page), headers=headers_compatibility)
    soup = BeautifulSoup (UNI.text , "lxml")
    
    links = soup.select("div.card > div.content > h2 > a")
    for link in links :
        scrapMediaDetail("https://www.themoviedb.org"+ link["href"],type)

class affiche :
    def __init__(self,id,linkImage,date_sortie,titre,type,saved) :
        self.id =id 
        self.linkImage =linkImage 
        self.date_sortie =date_sortie 
        self.titre =titre 
        self.type =type 
        self.saved = saved

    def get_titre(affiche):
        return affiche.titre
    
    def __str__(self) -> str:
        return self.titre

def recherche(recherche,movie,tv) :
    listMedia = list()

    if tv :
        headers_compatibility = {"User-Agent" : "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36"}
        UNI = requests.get(" https://www.themoviedb.org/search/tv?language=fr&query="+str(recherche), headers=headers_compatibility)
        soup = BeautifulSoup (UNI.text , "lxml")
        tv = soup.select("div.search_results div.card")
        listTV = list()
        
        for card in tv :
            titre = card.find("div", { "class" : "title" }).find("h2").text
            date = card.find("span", { "class" : "release_date" })
            if date :
                date = date.text
            else :
                date = ""
            image = card.find("div", { "class" : "poster" }).find("img")
            if image :
                image = ("https://www.themoviedb.org"+image["src"].replace("w94_and_h141_bestv2","w300_and_h450_bestv2"))
            else :
                image = "" 
            
            id = card.find("div", { "class" : "poster" }).find("a")
            id = id["href"][len("/tv/"):]
            id = id.replace('?language=fr','')

            try:
                media = Media.objects.get(id=id)
                saved = True
            except :
                saved = False

            listTV.append(affiche(id,image,date,titre,"tv",saved))
        listMedia.extend(listTV)

    if movie :
        headers_compatibility = {"User-Agent" : "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36"}
        UNI = requests.get(" https://www.themoviedb.org/search/movie?language=fr&query="+str(recherche), headers=headers_compatibility)
        soup = BeautifulSoup (UNI.text , "lxml")

        listMovie = list()
        movie = tv = soup.select("div.search_results div.card")
        for card in movie :
            titre = card.find("div", { "class" : "title" }).find("h2").text

            date = card.find("span", { "class" : "release_date" })
            if date :
                date = date.text
            else :
                date = ""
            image = card.find("div", { "class" : "poster" }).find("img")
            if image :
                image = ("https://www.themoviedb.org"+image["src"].replace("w94_and_h141_bestv2","w300_and_h450_bestv2"))
            else :
                image = "" 
            
            id = card.find("div", { "class" : "poster" }).find("a")
            id = id["href"][len("/movie/"):]
            id = id.replace('?language=fr','')

            try:
                media = Media.objects.get(id=id)
                saved = True
            except :
                saved = False

            listMovie.append(affiche(id,image,date,titre,"movie",saved))

        listMedia.extend(listMovie)
    

    return listMedia
        

    