# Projet Médiathèque

Le projet ```Médiathèque``` a pour but de mettre à disposition une liste de films, de séries, de jeux vidéos ainsi que de personnes ayant contribué ou participé à ces derniers. Ce dernier est réalisé dans le cadre d'un projet informatique à l' [IUT Informatique d'Orléans](https://www.univ-orleans.fr/fr/iut-orleans) et met en application le framework Django.

## Contributeurs

- [DEVOS Nicolas](https://gitlab.com/nicotoine)
- [PLOUY-ROSSARD Elie](https://gitlab.com/elieplr)
- [TALLAND Kevin](https://gitlab.com/kevintld)

## Installation

### > Pré-requis

```Python``` est nécessaire au bon fonctionnement du projet et sera donc considéré comme déjà installé sur le système source.

### > Environnement virtuel & Librairies

1. Dans un premier temps, mettez en place ```un environnement virtuel``` (optionnel mais recommandé) :
```bash
virtualenv -p python3 /path/you/want/name_of_env
```

2. Ensuite, activez votre environnement virtuel :
```bash
source /path/you/want/name_of_env/bin/activate
```

3. En considérant que vous êtes en possession du projet, rendez-vous à la racine de ce dernier :
```bash
cd /path/to/project
```

4. Procédez maintenant à l'installation des libraries requises au projet :
```bash
pip install -r requirements.txt
```

5. Si le projet est lancé depuis l'IUT, vous n'aurez rien a changé. En effet, nos données sont stockées dans une de nos sessions MySQL.</br>
En revanche, si vous lancez le projet depuis un autre endroit :

- Rendez-vous dans le fichier /path/to/project/Mediathèque/settings.py

- Décommentez les lignes suivantes:
```python
87 # 'ENGINE': 'django.db.backends.sqlite3'
...
95 # 'NAME': 'mediatheque.sqlite3'
```

- Commentez les lignes suivantes :
```python
88 'ENGINE': 'django.db.backends.mysql',
89 'OPTIONS': {
90    'read_default_file': os.path.join(BASE_DIR , 
      "identifiants.cnf"),
91  },
```

- Effectuez une migration :
```bash
python3 manage.py migrate
```

6. Lancez ensuite le serveur Django :
```bash
python3 manage.py runserver
```

## Fonctionnalités

### > Scraping

Pour enrichir notre base de données, nous avons récupéré nos informations directement depuis les sites [TheMovieDB](https://www.themoviedb.org/) pour nos Films, Séries et Personnes et [SensCritique](https://www.senscritique.com) pour nos Jeux Vidéos.

Nous disposons de plusieurs moyens de lancer ces scrapings :
- Via les commandes django :
```bash
# Films, Series, Personnes
python3 manage.py scrap 'movie' 1 
# où 1 est le numéro de page

# Jeux Vidéos
python3 manage.py scrapJV https://www.senscritique.com/jeuxvideo/tops/top111
```
- Via des scripts linux :
```bash
# Tout
./scrappingAll.sh

# Films, Personnes
./scrapMovie.sh

# Series, Personnes
./scrapTV.sh

# Jeux Vidéos
./scrapJV.sh
```

### > Globales

Depuis notre site il est possible que ce soit un Média, un Jeu Vidéo ou une Personne :
- d'ajouter (```administrateur```),
- de modifier (```administrateur```),
- de supprimer (```administrateur```),
- de rechercher et de trier l'entité ciblée
- de se connecter

Enfin, notre site est responsive.

## Tests

Des tests sont réalisés grâce à la librairie Selenium :
```bash
python3 manage.py createsuperuser
# username : root
# password : toor

python3 manage.py test
```
